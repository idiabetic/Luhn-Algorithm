# Implementation of the Lahn
class CreditCheck
  def valid_number?(number)
    array = splitter(number)
    check_num = array.pop
    array = doubler(array)
    array = reducer(array)
    array << check_num
    validator(array)
  end

  def valid?(number)
    valid_number?(number) ? 'Valid' : 'Invalid'
  end

  def validation_output(number)
    valid = valid_number?(number)
    "The number #{number} is #{valid ? 'valid' : 'invalid'}"
  end

  def get_check_digit(number)
    array = splitter(number)
    array = doubler_no_check(array)
    array = reducer(array)
    digit = array.sum.to_s.chars.map(&:to_i)
    # 10 - digit[-1]
  end

  def doubler_check(array)
    array.reverse.each_index do |index|
      array[index] = array[index] * 2 if (index % 2).zero?
    end
    return array
  end

  def doubler_no_check(array)
    array.reverse.each_index do |index|
      array[index] = array[index] * 2 if (index % 2).odd?
    end
    return array
  end

  def reducer(array)
    array.each_index do |index|
      array[index] = array[index].to_s.chars.map(&:to_i).sum if array[index] > 9
    end
    return array
  end

  def doubler(array)
    if array.length.even?
      doubler_no_check(array)
    else
      doubler_check(array)
    end
  end

  def validator(array)
    (array.sum % 10).zero?
  end

  def splitter(number)
    number.to_s.chars.map(&:to_i)
  end
end
