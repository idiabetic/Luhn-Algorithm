require_relative '../lib/credit_check'

# Interface for the CreditCheck Gem/Class.
class CreditCheckInterface
  def initialize
    puts 'Welcome to the Credit Card Number Validator:'
    @credit_card = user_input
  end

  def user_input
    puts 'Please enter the Credit Card Number'
    print '=> '
    gets.chomp
  end

  def credit_info
    credit_check = CreditCheck.new
    puts   '-----------------------------------------------------------------'
    puts   '|                            Credit Info                         '
    puts   '|----------------------------------------------------------------'
    puts   "| - Credit Card Number: #{@credit_card}                          "
    puts   "| - This card is #{credit_check.valid?(@credit_card)}            "
    puts   '|----------------------------------------------------------------'
  end
end

credit = CreditCheckInterface.new
credit.credit_info
