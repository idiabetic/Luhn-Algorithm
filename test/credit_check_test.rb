gem 'minitest', '~> 5.2'
require 'minitest/autorun'
require 'minitest/pride'
require_relative './credit_check'

# Tests the CreditCheck Class
class CreditCheckTest < Minitest::Test
  def test_it_can_be_created
    credit_check = CreditCheck.new
    assert_equal 'CreditCheck', credit_check.class.to_s
  end

  def test_it_doubles
    credit_check = CreditCheck.new
    doubled = credit_check.doubler_check([1, 2, 3, 4, 5])
    assert_equal [2, 2, 6, 4, 10], doubled
  end

  def test_it_reduces
    credit_check = CreditCheck.new
    reduced = credit_check.reducer([11, 12, 13, 14, 15])
    assert_equal [2, 3, 4, 5, 6], reduced
  end

  def test_it_validates
    credit_check = CreditCheck.new
    assert credit_check.valid_number?(5_541_808_923_795_240)
  end

  def test_it_invalidates
    credit_check = CreditCheck.new
    refute credit_check.valid_number?(5_541_801_923_795_240)
  end

  def test_it_validates_and_outputs
    credit_check = CreditCheck.new
    valid = credit_check.validation_output(5_541_808_923_795_240)
    assert_equal 'The number 5541808923795240 is valid', valid
  end

  def test_it_invalidates_and_outputs
    credit_check = CreditCheck.new
    valid = credit_check.validation_output(5_541_801_923_795_240)
    assert_equal 'The number 5541801923795240 is invalid', valid
  end

  def test_it_validates_amex
    credit_check = CreditCheck.new
    assert credit_check.valid_number?(342_804_633_855_673)
  end

  def test_it_invalidates_amex
    credit_check = CreditCheck.new
    refute credit_check.valid_number?(342_801_633_855_673)
  end

  def test_get_check_digit
    credit_check = CreditCheck.new
    assert 3, credit_check.get_check_digit(7_992_739_871)
  end
end
