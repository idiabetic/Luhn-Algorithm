# Luhn Algorithm

This is a simple implementation of the [Luhn Algorithm]( http://en.wikipedia.org/wiki/Luhn_algorithm)


### Usage:

```ruby
credit_check = CreditCheck.new
=> #<CreditCheck:0x00007fe82d2a8a38>
credit_check.valid_number?(5541808923795240)
=> true
credit_check.valid_number?(5541801923795240)
=> false
credit_check.validation_output(5541808923795240)
=> "The number 5541808923795240 is Valid"
credit_check.validation_output(5541801923795240)
=> "The number 5541801923795240 is Invalid"
credit_check.get_check_digit(7992739871)
=> 3
```

### CLI Tool
validator_interface.rb is a CLI tool to vereify credit card numbers.


#### Usage
```bash
ruby validator_interface.rb
Welcome to the Credit Card Number Validator:
Please select one of the following options:
1: Validate a number
2: Validate a number with output
3: Exit
=> 1
Please enter the Credit Card Number
=> 5541801923795240
The credit card number you entered is Invalid
Would you like to check another? y/n
=> n
```

---
# Self Eval - Score 4
# Questions

1. What are you proud of?
> That I was able to turn the project into a gem.
1. What do you need to improve?
> Using the correct enumerable methods and improving the single responsibility aspect
of my code
1. What questions do you still have?
> What is the proper way to create and maintain a gem.

# Rubric

## Functionality - 4

Notes:

- [x] Student completes through Iteration 3

## Mechanics - 4

Notes:

The student(s):

- [x] appropriately uses Strings, Integers, Floats, Ranges, Symbols, Nils, Arrays, and Hashes
- [x] implements best-choice enumerable methods to iterate over collections
- [x] uses boolean expressions and flow control structures to logically manage a program's flow
- [x] uses methods, arguments, and return values to break code into logical components
- [x] creates Classes that utilize instance variables, attribute accessors, and instance methods

## Design - 3.75

Notes:

The student(s):

- [ ] adheres to the Single Responsibility and DRY principles
- [x] creates Objects and Classes that appropriately encompass state and behavior
- [x] uses instance and local variables appropriately
- [x] writes readable code with the following characteristics:
    * Variable and method names are self explanatory
    * Methods are under 7 lines
    * Lines of code are under 80 characters
    * Project directory structure adheres to convention
    * A linter reports less than 5 errors

## Testing - 4

Notes:

The student(s):

- [x] writes Minitest tests that describe the expected behavior of a program according to technical specifications
- [ ] names and orders tests so that a test file reads like documentation
- [x] writes Minitest assertions that accurately test a piece of functionality
- [x] writes a test before writing code that implements the behavior to make that test pass
- [x] writes both integration and unit tests

## Version Control - 4

Notes:

The student(s):

- [x] hosts their code on the master branch of their remote repository
- [x] makes commits in small chunks of functionality
- [x] submits and merges Pull Requests using the Github interface
