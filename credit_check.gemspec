Gem::Specification.new do |s|
  s.name = 'credit_check'
  s.version = '0.0.1'
  s.date = '2018-06-27'
  s.summary = 'Implementation of Luhn Algorithm'
  s.files = [
    'lib/credit_check.rb'
  ]
  s.require_paths = ['lib']
  s.authors = 'Patrick Shobe'
end
